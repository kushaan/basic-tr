// Problem 1: Complete the secondLargest function which takes in an array of numbers in input and return the second biggest number in the array. (without using sort)?
function secondLargest(array) {
	let maxElement = array[0],
		secondMax = Number.MIN_SAFE_INTEGER;
	for (let iter in array) {
		if (array[iter] > maxElement) {
			maxElement = array[iter];
		}
	}
	for (let iter in array) {
		if (array[iter] < maxElement && array[iter] > secondMax) {
			secondMax = array[iter];
		}
	}
	return secondMax;
}

// Problem 2: Complete the calculateFrequency function that takes lowercase string as input and returns frequency of all english alphabet. (using only array, no in-built function)
function calculateFrequency(string) {
	let map = {};
	let tmp = "AZaz";
	for (let iter of string) {
		let asciiIter = iter.charCodeAt(0);
		let checkAlphabet =
			asciiIter >= tmp.charCodeAt(0) && asciiIter <= tmp.charCodeAt(1);
		checkAlphabet =
			checkAlphabet ||
			(asciiIter >= tmp.charCodeAt(2) && asciiIter <= tmp.charCodeAt(3));
		if (!checkAlphabet) continue;
		if (map[iter] === undefined) {
			map[iter] = 1;
			continue;
		}
		map[iter]++;
	}
	return map;
}

// Problem 3: Complete the flatten function that takes a JS Object, returns a JS Object in flatten format (compressed)
function flatten(unflatObject) {
	return solver(unflatObject, '');
}

function solver(unflatObject, prefix) {
	let result = {};
	for (let iter in unflatObject){
		if (typeof unflatObject[iter] === 'object'){
			let tmp;
			if (prefix === '')
				tmp = solver(unflatObject[iter], prefix+iter);
			else
				tmp = solver(unflatObject[iter], prefix+'.'+iter);
			result = Object.assign({}, result, tmp);
		}
		else{
			if (prefix === '')
				result[prefix+iter] = unflatObject[iter];
			else
				result[prefix+'.'+iter] = unflatObject[iter];
		}
	}
	return result;
}

// Problem 4: Complete the unflatten function that takes a JS Object, returns a JS Object in unflatten format

function unflatten(flatObject) {
	let result = {}; 
	for (let iter in flatObject){
		let keyList = iter.split('.');
		let tmp = result;
		for (let key = keyList.length; key >= 0; key--){
			if (tmp[key] === 'undefined')
				tmp[key] = {};
			tmp = tmp[key]
		}
		tmp[keyList[keyList.length-1]] = flatObject[iter];
	}
	return result;
}
